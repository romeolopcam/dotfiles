#!/bin/bash

# Se muestra un diálogo de Zenity para que el usuario ingrese la extensión deseada
extension=$(zenity --entry --title="Copia de archivos" --text="Ingrese la extensión de archivo a copiar (sin el punto):")

# Se muestra un diálogo de Zenity para que el usuario seleccione la ruta de destino
destination=$(zenity --file-selection --directory --title="Carpeta de destino")

# Se crea un directorio con el nombre de la extensión ingresada en la ruta seleccionada por el usuario
mkdir "$destination/$extension"

# Se utiliza el comando find para buscar archivos con la extensión especificada
# y se copian al directorio recién creado
find . -type f -iname "*.$extension" -exec cp {} "$destination/$extension" \;

# Se muestra una notificación de Zenity al completar la copia de archivos
zenity --info --title="Copia de archivos" --text="Se han copiado los archivos de extensión .$extension al directorio $destination/$extension." --width=400