#!/bin/bash
# BSDL Miro Hrončok <miro@hroncok.cz>
# subrnm .avi .srt (or vice versa)

if [ $# -ne 2 ]; then
    #echo "USAGE: subrnm .avi .srt (or vice versa)"
    zenity --error --width=200 --height=50 --text "Selecciona dos archivos."
    exit 1
fi

# Which is which
filename=$(basename "$1")
extension=${filename##*.}

# Video first
if [ "$extension" == "avi" ] || [ "$extension" == "mp4" ] || [ "$extension" == "mkv" ]; then
    #echo "First file is a video"
    subtitles=$(basename "$2")
    extension=${subtitles##*.}
    if [ "$extension" != "srt" ] && [ "$extension" != "sub" ]; then
        #echo "First file is a video. Second file are not subtitles"
        zenity --error --width=200 --height=50 --text "El primer archivo es un video, pero el segundo no es un subtítulo."
        exit 1
    fi
    filename=${filename%.*}
    dir=$(dirname "$1")
    mv "$2" "$dir/$filename.$extension"
    exit 0
fi

# Subtitles first
if [ "$extension" == "srt" ] || [ "$extension" == "sub" ]; then
    #echo "First file are subtitles"
    video=$(basename "$2")
    videext=${video##*.}
    if [ "$videext" != "avi" ] && [ "$videext" != "mp4" ] && [ "$videext" != "mkv" ]; then
        #echo "First file are subtitles. Second file in not a video"
        zenity --error --width=200 --height=50 --text "El primer archivo es un subtítulo, pero el segundo no es un video."
        exit 1
    fi
    video=${video%.*}
    dir=$(dirname "$2")
    mv "$1" "$dir/$video.$extension"
    exit 0
fi

#echo "USAGE: subrnm .avi .srt (or vice versa)"
zenity --error --width=200 --height=50 --text "El primer archivo no es un video ni un subtítulo."
exit 1
