#!/bin/bash

# Script para convertir archivos PDF seleccionados a imágenes PNG

# Bucle para iterar sobre cada archivo pasado como argumento
for file in "$@"
do
    # Verificar si el archivo tiene la extensión .pdf
    if [[ "${file##*.}" == "pdf" ]]; then
        # Obtener el nombre base del archivo sin la extensión .pdf
        output="$(basename "$file" .pdf).png"
        # Ejecutar pdftoppm para convertir la primera página a PNG
        pdftoppm -png -f 1 -l 1 -scale-to 300 "$file" > "$output"
    fi
done
